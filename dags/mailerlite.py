from airflow import DAG
from airflow.operators.python import PythonOperator
from airflow.models import Variable

from datetime import datetime

from conectabbdd import conecta


def _getmailerlitedata():
    return conecta.getmailerlitedata()


with DAG("DAG_mailerlite", start_date=datetime(2021, 1, 1), schedule_interval="0 4 * * *", catchup=False) as dag:
    conecta_bbdd_insert = PythonOperator(
        task_id="get_mailerlite_data",
        python_callable=_getmailerlitedata,
        email_on_failure=True,
        email=Variable.get("mail_zulip")
    )

    conecta_bbdd_insert

